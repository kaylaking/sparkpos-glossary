# Summary

* [Introduction](README.md)
* [报表](bao-biao.md)
* [营运报表](ying-yun-bao-biao.md)
  * [区域热销产品](ying-yun-bao-biao/qu-yu-re-xiao-chan-pin.md)
  * [时段营业额报告](ying-yun-bao-biao/shi-duan-ying-ye-e-bao-gao.md)
  * [成本明细](ying-yun-bao-biao/cheng-ben-ming-xi.md)
  * [产品售卖量报告](ying-yun-bao-biao/chan-pin-shou-mai-liang-bao-gao.md)
* [财务报表](cai-wu-bao-biao.md)
  * [营业报告表](cai-wu-bao-biao/ying-ye-bao-gao-biao.md)
  * [优惠折扣明细](cai-wu-bao-biao/you-hui-zhe-kou-ming-xi.md)
  * [营业收入明细](cai-wu-bao-biao/ying-ye-shou-ru-ming-xi.md)

